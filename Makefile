DATE=`date +%Y-%m-%d---%H-%M-%S`

ctan:
	mkdir -p dist/biblatex-ajc2020unofficial
	rm -rf dist/biblatex-ajc2020unofficial/*
	cp -t dist/biblatex-ajc2020unofficial *.md *.bbx *.cbx
	cd dist; zip -r biblatex-ajc2020unofficial---$(DATE).zip biblatex-ajc2020unofficial
